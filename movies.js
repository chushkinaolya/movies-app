const movies = [
  {
    id: 1,
    title: "Человек и черепаха",
    genres: [
      {
        id: 1,
        name: "Фантастика",
      },
      {
        id: 2,
        name: "Комедия",
      },
    ],
    release_date: "12.12.1990",
    runtime: 100,
    overview: "Фильм о дружбе человека и черепахи",
    poster_path: "",
  },
  {
    id: 2,
    title: "Спартак",
    genres: [
      {
        id: 1,
        name: "Драма",
      },
      {
        id: 2,
        name: "Исторический",
      },
    ],
    release_date: "15.01.2022",
    runtime: 120,
    overview: "Фильм по мотивам мифов о Спартаке",
    poster_path: "",
  },
  {
    id: 3,
    title: "Детство",
    genres: [
      {
        id: 1,
        name: "Мелодрама",
      },
      {
        id: 2,
        name: "Комедия",
      },
    ],
    release_date: "09.10.2001",
    runtime: 1,
    overview: "Добрая комедия о детстве",
    poster_path: "",
  },
  {
    id: 4,
    title: "Аппокалипсис",
    genres: [
      {
        id: 1,
        name: "Фантастика",
      },
      {
        id: 2,
        name: "Драма",
      },
    ],
    release_date: "06.07.2010",
    runtime: 95,
    overview: "Фантазии на тему конца света",
    poster_path: "",
  },
  {
    id: 5,
    title: "Любовь",
    genres: [
      {
        id: 1,
        name: "Мелодрама",
      },
    ],
    release_date: "23.09.2002",
    runtime: 100,
    overview: "Красивая история любви",
    poster_path: "",
  },
  {
    id: 6,
    title: "Кошмар",
    genres: [
      {
        id: 1,
        name: "Ужасы",
      },
    ],
    release_date: "13.11.2012",
    runtime: 85,
    overview: "История, леденящая кровь",
    poster_path: "",
  },
  {
    id: 7,
    title: "18 врагов",
    genres: [
      {
        id: 1,
        name: "Комедия",
      },
    ],
    release_date: "10.11.2013",
    runtime: 134,
    overview: "Приключения незадачливых друзей",
    poster_path: "",
  },
];

export default movies;
