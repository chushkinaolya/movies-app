import type { H3Event } from "h3";
import type { Movie } from "~/types/Movie";
import type { APIResponse } from "~/types/APIResponse";
import movies from "~/movies";

export default defineEventHandler((event: H3Event) => {
  const ITEMS_ON_PAGE = 2;

  const { query, page } = getQuery(event);

  if (query && page) {
    const filteredMovies: Movie[] = movies.filter((m: Movie): boolean =>
      m.title.toLowerCase().includes((query as string).toLowerCase()),
    );

    const response: APIResponse = {
      page: +page,
      results: query
        ? filteredMovies.slice(
            (+page - 1) * ITEMS_ON_PAGE,
            (+page - 1) * ITEMS_ON_PAGE + ITEMS_ON_PAGE,
          )
        : [],
      total_pages: Math.ceil(filteredMovies.length / ITEMS_ON_PAGE),
      total_results: filteredMovies.length,
    };

    return response;
  }

  return null;
});
