import type { H3Event } from "h3";
import type { Movie } from "~/types/Movie";
import movies from "~/movies";

export default defineEventHandler((event: H3Event) => {
  const movieId = event.node.req.url?.split("/").pop();

  if (movieId) {
    const movie: Movie | undefined = movies.find(
      ({ id }: Movie) => id === +movieId,
    );
    return movie;
  }

  return null;
});
