import type { Genre } from "~/types/Genre";

export type Movie = {
  id: number;
  title: string;
  genres: Genre[];
  release_date: string;
  runtime: number | null;
  overview: string;
  poster_path: string;
};
